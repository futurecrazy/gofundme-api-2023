import express from 'express';

import puppeteer from 'puppeteer';

const app = express();
import cors from 'cors';

import { collection, doc, setDoc, getDocs, writeBatch, serverTimestamp } from "firebase/firestore";
import { db } from "./config/firebase.js"

import lodash from 'lodash';
const { _ } = lodash;

app.listen(process.env.PORT, () => console.log("Server Start at Port"));

app.use(express.static('public'));
app.use(cors({
    origin: ['http://localhost:3000']
}));

app.get('/api', getData);

async function getData(request, response) {

    console.log(request.query.url);

    const browser = await puppeteer.launch({
        headless: true,
        'args': [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--lang=en-GB,en'
        ]
    }); // for test disable the headlels mode,
    const page = await browser.newPage();

    page.setDefaultNavigationTimeout(0);

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4427.0 Safari/537.36');
    await page.setViewport({ width: 1000, height: 926 });

    let url = 'https://www.gofundme.com/charity/razom-razom-for-ukraine?modal=donations';
    await page.goto(url, { waitUntil: 'domcontentloaded' });
    await page.waitForSelector('#global-wrapper', { timeout: 0 });

    console.log("start evaluating javascript");

    // evaluate the dom and scrape the data

    var dataFromDom = await page.evaluate(() => {

        let modal = document.querySelector(".a-overlay--open .list-unstyled");
        let donationsList = modal.querySelectorAll(":scope > li");

        var data = {
            donations: [],
        };


        for (let index = 0; index < donationsList.length; index++) {

            function name() {
                let nameElement = donationsList[index].querySelector(".m-person-info-name");
                if (nameElement) {
                    return nameElement.textContent;
                } else {
                    return "";
                }
            }

            function amount() {
                let amountElement = donationsList[index].querySelector(".m-person-info-content .weight-900");
                if (amountElement) {
                    return amountElement.textContent;
                } else {
                    return "";
                }
            }

            let recName = name();
            let recAmount = amount();

            if (recName != "" && recAmount != "") {
                
                data.donations.push({
                    id: recName + recAmount,
                    name: recName,
                    amount: recAmount,
                });
            }
        }
        return data
    });

    
    // get donations from the db. New donations are recorded to the Firebase db down below
    // as we can only fetch the last recent 21 donations from gofundme

    const querySnapshot = await getDocs(collection(db, "donations"));

    let donationsFromDb = [];

    querySnapshot.forEach((doc) => {
        donationsFromDb.push(doc.data());
    });


    // ======== only run this once to populate the db initially if required 

    // const batch = writeBatch(db);

    // for (let index = 0; index < dataFromDom.donations.length; index++) {
    //     const nycRef = doc(db, "donations", Date.now()+"_"+index);
    //     batch.set(nycRef, {
    //         id: dataFromDom.donations[index].name + dataFromDom.donations[index].amount,
    //         name: dataFromDom.donations[index].name,
    //         amount: dataFromDom.donations[index].amount
    //     });
    // }

    // await batch.commit();

    // ======== only run this once to populate the db initially 


    console.log("donationsFromDb =======");
    console.log(donationsFromDb);
    console.log("dataFromDom =======");
    console.log(dataFromDom);

    
    // determine if any new donations were made since the script was last run 
    // by comparing the list of donations from the Firebase database with the one scraped from dom  
    let newDonations = _.differenceBy(dataFromDom.donations, donationsFromDb, 'id');

    console.log("======= Diff: ");
    console.log(newDonations);

    // if the new donations were made on gofundme, add them to the external Firebase db 
    if(newDonations.length > 0) {
        const batchAdditional = writeBatch(db);

        for (let index = 0; index < newDonations.length; index++) {
            const nycRef = doc(db, "donations", Date.now()+"_"+index);
            batchAdditional.set(nycRef, {
                id: newDonations[index].name + newDonations[index].amount,
                name: newDonations[index].name,
                amount: newDonations[index].amount
            });
        }

        await batchAdditional.commit();
    }


    browser.close()

    var reply = donationsFromDb.concat(newDonations);

    response.send(reply);

}